## 介绍
网市场云建站系统-活动中免费给别人部署，就是部署的这个，wangmarket本身以jar形式存在，以便升级

# 线上部署
#### 1. 服务器部署
* 服务器系统：CentOS 7.4（7.0~7.6应该都行，如果有7.4，请选7.4，其他版本只是猜测应该没问题）
* tomcat8.5 安装:  https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3690921&doc_id=1101390  
* mysql 5.7 （请注意版本，必须5.7！必须！别用8.0啥的）安装：https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3690920&doc_id=1101390  
* mysql数据库文件下载：https://gitee.com/mail_osc/wangmarket/raw/master/else/wangmarket.sql  

#### 2. 配置数据库连接
参考： https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3634642&doc_id=1101390

#### 3. 启动，使用
启动成功后，访问 /login.do 按照安装步骤提示及说明进行操作

#### 4. 版本更新
直接更新 WEB-INF/lib/wangmarket-xxx.jar 即可完成更新 (有时还会更新其他jar包，到时详细查阅更新说明里需要更新的jar包即可。)

## 二次开发及扩展
#### 原本的页面及功能修改、重写
比如，感觉原本的登录页面不好看，那么可以对登录页进行重写。重写方式参考： https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3631374&doc_id=1101390  
wangmarket 的源码文件在项目：  https://gitee.com/mail_osc/wangmarket  

#### 定制开发自己想要的功能
参考：  https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3213258&doc_id=1101390